package com.cognizant.iot.welcomescreen;


import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {

    TextView name, points;
    ImageView profile_settings, search;
    Button view_profile, social_vibes;
    MapFragment mapFragment;
    LatLng current, destination;
    GoogleMap googleMap;
    Double latitude, longitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name = (TextView) findViewById(R.id.name);
        points = (TextView) findViewById(R.id.points);
        profile_settings = (ImageView) findViewById(R.id.profile_settings);
        search = (ImageView) findViewById(R.id.search);
        view_profile = (Button) findViewById(R.id.view_profile);
        social_vibes = (Button) findViewById(R.id.social_vibes);


        profile_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Profile Settings", Toast.LENGTH_SHORT).show();
            }
        });
        view_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "View Profile", Toast.LENGTH_SHORT).show();
            }
        });

        social_vibes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Social Vibes", Toast.LENGTH_SHORT).show();
            }
        });


        //autocomplete textview setting up
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, STORES);
        final AutoCompleteTextView search_store = (AutoCompleteTextView)
                findViewById(R.id.search_store);
        search_store.setAdapter(adapter);


        //new destination search
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show_users_store(search_store.getText().toString());                    //setting picker to destination

                InputMethodManager imm = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(search.getApplicationWindowToken(), 0);
            }
        });

        //current location tracking

//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                int MY_LOCATION_PERMISSION_CODE = 101;
//                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_LOCATION_PERMISSION_CODE);
//            }
//            return;
//        }
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        Location location = locationManager.getLastKnownLocation(provider);

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(
                R.id.map);
        mapFragment.getMapAsync(this);

        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            //map setting
            current = new LatLng(latitude, longitude);

        } else {
            //This is what you need:
            locationManager.requestLocationUpdates(provider, 1000, 0, this);
        }
    }

    private void show_users_store(String store_name) {
        //if user searches without giving any input then picker will stay at it's initial position
        Double lat = latitude, lon = longitude;


        try {
            JSONObject stores = new JSONObject(loadLocationJSONFromAsset());
            JSONArray all_stores = stores.getJSONArray("locations");
            for (int i = 0; i < all_stores.length(); i++) {
                JSONObject single_store = all_stores.getJSONObject(i);
                if (single_store.getString("place").equals(store_name)) {
                    lat = single_store.getDouble("lat");
                    lon = single_store.getDouble("lng");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        destination = new LatLng(lat, lon);
        CameraPosition cameraPosition = new CameraPosition.Builder().target(destination).zoom(15.0f).build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        googleMap.moveCamera(cameraUpdate);
    }


    private String loadLocationJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getApplicationContext().getAssets().open("location.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.e("JSONLOCATION", json);
        return json;
    }


    private static final String[] STORES = new String[]{                                   //Autocomplete Textview String array making
            "Houston", "Amsterdam", "AjmanUAE", "Mumbai", "Chennai"
    };

    @Override
    public void onMapReady(GoogleMap Map) {
        googleMap = Map;
        Log.e("poision", current.toString());
        googleMap.addMarker(new MarkerOptions().position(current).title("Your Current Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        googleMap.addMarker(new MarkerOptions().position(new LatLng(29.739549, -95.462716)).title("Houston").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        googleMap.addMarker(new MarkerOptions().position(new LatLng(52.372912, 4.892269)).title("Amsterdam").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        googleMap.addMarker(new MarkerOptions().position(new LatLng(25.403023, 55.491797)).title("AjmanUAE").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        googleMap.addMarker(new MarkerOptions().position(new LatLng(18.975, 72.8258)).title("Mumbai").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        googleMap.addMarker(new MarkerOptions().position(new LatLng(13.0827, 80.2707)).title("Chennai").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(current).zoom(15.0f).build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        googleMap.moveCamera(cameraUpdate);

    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
